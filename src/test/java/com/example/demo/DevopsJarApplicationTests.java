package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.utils.CalendarUtil;
import com.example.demo.utils.StringUtil;



@SpringBootTest
class DevopsJarApplicationTests {
	
	
    private  CalendarUtil calendar = new CalendarUtil();
    private  StringUtil util = new StringUtil();



	@Test
	void convertDateEnglishToLocal() throws ParseException {
		
        assertEquals(calendar.getDateOperation("January 2, 2010"), "02 Ene 2010");
	}
	
	@Test
	void convertHourEnglishToLocal() throws ParseException {
		
				
        assertEquals(calendar.getTimeOperation("January 2, 2010 11:59:00"), "11:59 AM");
	}
	
	
	@Test
	void firtName() throws ParseException {
				
        assertEquals(util.getFirstName("Luis Gustavo Lozano"), "Luis");
	}
	
	@Test
	void firtName2() throws ParseException {
				
        assertEquals(util.getFirstName("Gustavo Gustavo Lozano"), "Gustavo");
	}
	

	@Test
	void cciFormat() throws ParseException {
		
        assertEquals(util.cciFormat("12345678901234567890"), "123-456-789012345678-90");
	}
	
	@Test
	void cciFormat2() throws ParseException {
		
        assertEquals(util.cciFormat("12345123451234567890"), "123-451-234512345678-90");
	}
	

}
