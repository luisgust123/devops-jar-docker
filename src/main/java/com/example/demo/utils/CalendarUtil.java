package com.example.demo.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CalendarUtil {

	public static final String PATTERN_DATE_DD_MMM_YYYY = "dd MMM yyyy";
	public static final String PATTERN_DATE_YYYY_MMM_DD = "yyyyMMdd";
	public static final String PATTERN_HOUR_HH_MM_A = "hh:mm a";
	public static final String PATTERN_DATE_DD_MM_YYYY = "dd/MM/yyyy";
	public static final String SPACE = " ";

	

	public String getDateOperation(String  today ) throws ParseException {
		
		
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		Date date2 = format.parse(today);
				
		DateFormat dateFormat = new SimpleDateFormat(PATTERN_DATE_DD_MMM_YYYY);
		
		//Date today = Calendar.getInstance().getTime();
		String reportDate = dateFormat.format(date2);

		String[] val = reportDate.split(" ");
		String mes = val[1].substring(0, 1).toUpperCase().concat(val[1].substring(1));
		mes = getMonth(mes);

		return val[0].concat(SPACE).concat(mes).concat(SPACE).concat(val[2]);
	}

	public String getTimeOperation(String  today ) throws ParseException {
		
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy HH:mm:ss", Locale.ENGLISH);
		Date date2 = format.parse(today);
		
		
		DateFormat dateFormat = new SimpleDateFormat(PATTERN_HOUR_HH_MM_A);
		//Date today = Calendar.getInstance().getTime();
		
		
		return dateFormat.format(date2);
	}



	public static String getMonth(String mes) {
		switch (mes) {
		case "Jan":
			mes = "Ene";
			break;
		case "Apr":
			mes = "Abr";
			break;
		case "Aug":
			mes = "Ago";
			break;
		case "Sep":
			mes = "Set";
			break;
		case "Dec":
			mes = "Dic";
		default:
			break;
		}
		return mes;
	}

}
