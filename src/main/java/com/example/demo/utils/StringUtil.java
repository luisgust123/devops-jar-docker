package com.example.demo.utils;

public class StringUtil {
	
	public static String getFirstName(String fullName) {
		return (fullName.indexOf(" ") != -1) ? fullName.substring(0, fullName.indexOf(" ")) : fullName;
	}
	
	public static String cciFormat (String cci) {	
		return cci.substring(0, 3) + "-"+ cci.substring(3, 6) + "-"+cci.substring(6, 18) + "-"+cci.substring(18, 20) ;
	}
	
	public static String codClientFormat(String valor) {
		return String.format("%014d", Integer.parseInt(valor));
	}

}