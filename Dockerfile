FROM openjdk:8-slim
LABEL maintainer="luisgust123"
WORKDIR /workspace
RUN ls -la /workspace
COPY /target/api*.jar app.jar
RUN ls -la /workspace
EXPOSE 8090
ENTRYPOINT exec java -jar /workspace/app.jar